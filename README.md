# R package: VRAP2 
An amendment to the VRAP package developed by staff at the US National Marine Fisheries Service (NMFS) within the National Oceanic and Atmospheric Administration (NOAA). Running the following two lines in R will install the `devtools` package from CRAN, then install `VRAP2` from github. Not all functions have been fully tested. 
```{r} 
install.packages("devtools") 
devtools::install_git("https://gitlab.com/MichaelFolkes/VRAP2") 
```

Once the package is installed and loaded a generic script to guide the user can be created by running the function `writeScriptVRAP()`.

